from database import Database as db
from template import Template as tmpl
from keys import *
from settings import *
from telegram.ext import Updater
import datetime as dt
import random


def set_logging( set ):
    """
    Logs bot traffic into console
    """
    if( set ):
        import logging
        logging.basicConfig(level=logging.DEBUG,
                 format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

def start(update, context):
    """
    Bot's start command. Responds with a greeting.
    """
    chat_id = update.effective_chat.id
    text    = tmpl.messages['start']
    context.bot.send_message(chat_id, text)

def newbook(update, context):
    """
    Telegram bot command to print top list based on hours
    """
    chat_id = update.effective_chat.id
    user    = update.message.from_user.username

    if not user:
        text  = tmpl.messages['no_username']
        context.bot.send_message(chat_id, text)
    args = update.message.text.split(' ')
    args.pop(0) 
    pages = args[0]
    args.pop(0) 
    book = ' '.join( args )
    
    statement = "(telegram_user, pages, book_name) values(%s,%s,%s)"
    values = (user, pages, book )
    table = 'books'
    db.write( table, statement, values )
    text = tmpl.messages['book_confirm'].format( user, book )

    if book == 'Eroon oireista' or book == 'eroon oireista': 
        text = tmpl.messages['book_nordin_confirm'].format( user, book )
    context.bot.send_message(chat_id, text)


def dayoff(update, context):
    """
    Telegram command for setting the day off
    """
    chat_id = update.effective_chat.id
    user    = update.message.from_user.username
    text = tmpl.messages['day_off'].format( user )
    args = update.message.text.split(' ')
    daytype = 1
    if args[0] == '/kipee':
        daytype = 2
        text = tmpl.messages['day_off_sick'].format( user )
    elif not can_have_day_off( user ):
        text    = tmpl.messages['no_day_off']
        context.bot.send_message(chat_id, text)
        return False
    
    statement = "(telegram_user, daytype) values(%s, %s)"
    values = (user, daytype)
    table = 'dayoff'
    db.write( table, statement, values )
    context.bot.send_message(chat_id, text)

def can_have_day_off( user ):
    daysoff = db.read( 'dayoff', 'time_of_logging', 'telegram_user="' + user + '" AND daytype=1')

    if not daysoff:
        return True

    last_day_off = daysoff[-1][0]
    current_day = dt.datetime.now()
    last_day_off_week = last_day_off.isocalendar()[1]
    current_week = current_day.isocalendar()[1]

    return current_week > last_day_off_week

def log_with_time(update, context):
    """
    Telegram command for logging sport and reading progression

    telegram_user,
    progress,
    comment,
    rowtype, 1=sports, 2=book

    """
    chat_id = update.effective_chat.id
    user = update.message.from_user.username
    if not user:
        text  = tmpl.messages['no_username']
        context.bot.send_message(chat_id, text)
    date = update.message.date
    args = update.message.text.split(' ')
    rowtype = 1
    book = ''
    if args[0] == '/luin' or args[0] == '/luin_silloin' :
        rowtype = 2
    args.pop(0)
    date = args[0]
    args.pop(0)
    progress = args[0]
    args.pop(0)
    comment = ' '.join( args )
    statement = "(time_of_logging, telegram_user, progress, comment, rowtype) values(%s,%s,%s,%s,%s)"
    text = tmpl.messages['log_confirm_1_date' ].format( user, progress, date )

    if rowtype == 2:
        book = get_current_read( user, comment )
        print( book )
        read_pages = book['progress'] + int( progress )
        pages = book['pages']
        if not comment:
            comment = book['name']
        if read_pages >= pages:
            text = tmpl.messages['log_confirm_3'].format( user, progress, comment, pages, read_pages )
        else:
            text = tmpl.messages['log_confirm_2_date'].format( user, progress, comment, pages, read_pages, date )
    values = (date+' 12:00:00', user, progress, comment, rowtype)
    table = 'logs'
    db.write( table, statement, values )
    context.bot.send_message(chat_id, text)

def log(update, context):
    """
    Telegram command for logging sport and reading progression
    
    telegram_user,
    progress,
    comment,
    rowtype, 1=sports, 2=book

    """
    chat_id = update.effective_chat.id
    user = update.message.from_user.username
    if not user:
        text  = tmpl.messages['no_username']
        context.bot.send_message(chat_id, text)
    date = update.message.date
    args = update.message.text.split(' ')
    rowtype = 1
    book = ''
    if args[0] == '/luin':
        rowtype = 2
    args.pop(0) 
    progress = args[0]
    args.pop(0)
    comment = ' '.join( args )  
    statement = "(telegram_user, progress, comment, rowtype) values(%s,%s,%s,%s)"
    text = tmpl.messages['log_confirm_1' ].format( user, progress )

    if rowtype == 2:
        book = get_current_read( user, comment )
        print( book )
        read_pages = book['progress'] + int( progress )
        pages = book['pages']
        if not comment:
            comment = book['name']
        if read_pages >= pages:
            text = tmpl.messages['log_confirm_3'].format( user, progress, comment, pages, read_pages )
        else:
            text = tmpl.messages['log_confirm_2'].format( user, progress, comment, pages, read_pages )
    values = (user, progress, comment, rowtype)
    table = 'logs'
    db.write( table, statement, values )
    context.bot.send_message(chat_id, text)

def get_current_read( user, comment ):
    book = {}
    if not comment:
        books = db.read( 'books', 'pages, book_name', 'telegram_user="' + user + '"' )
        book['pages'] = books[-1][0]
        book['name']  = books[-1][1]
    else: 
        books = db.read( 'books', 'pages, book_name', 'telegram_user="' + user + '" AND book_name="' + comment + '"' )
        book['pages'] = books[-1][0]
        book['name'] = comment
    progresses = db.read( 'logs', 'progress', 'telegram_user="' + user + '" AND comment="'+ book['name'] +'"' )
    if not progresses:
        book['progress'] = 0
    else:
        book['progress'] = sum(map(sum, progresses ) )
    return book

def morning_job(update, context):
    """ Running on Mon, Tue, Wed, Thu, Fri = tuple(range(5)) """
    context.bot.send_message( -502976381, text='Jahas, huomenna sit aamutervehdystä.')
    t = dt.time(4, 30, 00, 000000)
    context.job_queue.run_daily(aamukongi, t, days=tuple(range(7)), context=update)

def aamukongi( context ):
    delta = dt.datetime.now() - dt.datetime(2021, 3, 14)    
    day = delta.days
    text = tmpl.messages['morning'].format( day )
    context.bot.send_message( settings['group_id'], text)

def evening_job( update, context ):
    """ Running on Mon, Tue, Wed, Thu, Fri = tuple(range(5)) """
    context.bot.send_message(-502976381, text='Erityistarkkailija ilmoittautuu palveluksen')
    t = dt.time(17, 00, 00, 000000)
    context.job_queue.run_daily(iltanuotio, t, days=tuple(range(7)), context=update)

def iltanuotio( context ):

    exercise_velaatat = get_velaatat( 1 )
    reading_velaatat = get_velaatat( 2 )

    debt = ''
    if exercise_velaatat:
        debt += 'liikunnassa: @' + ', @'.join(exercise_velaatat)
    if reading_velaatat:
        debt += ' lukemisessa: @' + ', @'.join(reading_velaatat)
    if not exercise_velaatat and not reading_velaatat:
        debt = 'ei ketään. Hyvää työtä!'

    text = tmpl.messages['no_log_today'].format(debt)
    context.bot.send_message( settings['group_id'], text )

def iltarovio( update, context ):
    exercise_velaatat = get_velaatat( 1 )
    reading_velaatat = get_velaatat( 2 )
    
    if not exercise_velaatat and not reading_velaatat:
        return True

    velaatat = set( exercise_velaatat + reading_velaatat )
    dayoff_velaatat = []

    for user in velaatat:
        if not can_have_day_off( user ):
            put_in_velka( user, context )
        else: 
            time_of_logging = dt.datetime.today() - dt.timedelta(days = 1) 
            statement = "(time_of_logging, telegram_user, daytype) values(%s, %s, %s)"
            values = (time_of_logging, user, 1)
            table = 'dayoff'
            db.write( table, statement, values )
            dayoff_velaatat.append( user )
            
    if dayoff_velaatat:
        text = 'Ja, ilmoittamatta vapaapäivää eilen vietti: @' + ', @'.join(dayoff_velaatat)
        context.bot.send_message( -502976381, text )


def put_in_velka( user, context ):
    context.bot.send_message( -502976381, '@' + user + ' viettää huomenna haasteen ekaa päivää')


def get_velaatat( logtype ):
    logs = db.read('logs', 'telegram_user,progress', 
        'time_of_logging >= CURDATE() AND time_of_logging < CURDATE() + INTERVAL 1 DAY and rowtype = ' + str(logtype) )

    amounts = {}

    for player in players:
        amounts[ player ] = 0

    velaatat = players.copy()

    for log in logs:
        amounts[ log[0] ] += log[1]
        if ( logtype == 2 and amounts[ log[0] ] >= 10 ) or ( logtype == 1 and amounts[ log[0] ] >= 45 ):
            if log[0] in velaatat:
                velaatat.remove( log[0] )

    on_dayoff = db.read('dayoff', 'telegram_user', 
        'time_of_logging >= CURDATE() AND time_of_logging < CURDATE() + INTERVAL 1 DAY')

    for vacationer in on_dayoff:
        if vacationer[0] in velaatat:
            velaatat.remove( vacationer[0] )
    
    return velaatat

def image_handler( update, context ):
    file = update.message.effective_attachment.get_file()
    user = update.message.from_user.username
    file.download('images/' + user + 'image.jpg')
    print( file )

def create_bot( handlers ):
    """
    Add all message handlers and initialize the bot
    """
    from telegram.ext import CommandHandler
    from telegram.ext import MessageHandler
    from telegram.ext import Filters

    token =  telegram_token ## Imported from keys.py
    updater = Updater(token, use_context=True)
    dispatcher = updater.dispatcher
    dispatcher.add_handler(CommandHandler('vesaesa', evening_job))
    dispatcher.add_handler(CommandHandler('esavesa', morning_job))
    # dispatcher.add_handler(MessageHandler(Filters.photo, image_handler))

    for command, messageHandler in handlers.items():
        handler = CommandHandler(command, messageHandler)
        dispatcher.add_handler(handler)
    return updater

from telegram.ext import Updater, CommandHandler

handlers = {
    'sitvaa': start, 
    'kirja': newbook, 
    'luin': log,
    'luin_silloin': log_with_time,
    'liikuin': log,
    'liikuin_silloin' : log_with_time,
    'vapaa' : dayoff,
    'kipee': dayoff,
}

set_logging(settings['logging'])
bot = create_bot( handlers )

bot.start_polling()
