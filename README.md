# PALJUBOT

@MaukanPaljuBot

🍵 +💧 + 🔥= 😎

## Commands
format: `command` `[explanation:required argument]` `(optional argument)`
* `/start` - Return a greeting
* `/paljutop` `(top list:x)`  - Returns your palju time and top list. Default is top 3.
    * example: `/paljutop 5` - prints your results and top 5 list.
    * example: `/paljutop 0` - prints your results only.
* `/paljua` `[hours:x.xx]` `(date:d.m.Y)` - Log your time in Maukan palju
    * example: `/paljua 2.5 5.1.2020` - logs 2.5 hours at 5.1.2020 date

## Coming in v2
* `/paljuun` - start logging your time in palju
* `/paljusta` - stop logging your time in palju
* `/millonpalju` - get information when palju is
