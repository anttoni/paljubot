from operator import itemgetter
class Template:

    messages = {
        'start': 'Tämä on 75 semi hard haaste 75 semi hard. 75 päivää. Sit vaan',
        'log_confirm_1': 'Kirjattu {1} minuuttia liikuntaa käyttäjälle {0}.',
        'log_confirm_2': 'Kirjattu {1} sivua käyttäjälle {0}. Kirjaa {2} luettu {4}/{3}.',
        'log_confirm_3': 'Sivun {1} lukemisella käyttäjä {0} ja kirja {2} lähtevät eri suuntiin!',
        'log_confirm_1_date': 'Kirjattu {1} minuuttia liikuntaa käyttäjälle {0}, päivälle {2}.',
        'log_confirm_2_date': 'Kirjattu {1} sivua käyttäjälle {0}, päivälle {5}. Kirjaa {2} luettu {4}/{3}.',
        'no_log_today': 'Erityistarkkailussa {0}',
        'no_username': 'Käyttäjänimi puuttuu ja se vaaditaan botin käyttöön. Aseta käyttäjänimi profiilissasi.',
        'morning': 'Huomenta, jahas, miten sä siihen oot päätynyt nukkumaan? Tänään on hasteen {0}. päivä.',
        'no_day_off': 'Hetkinen, seis! Sinulla on jo ollut tällä viikolla vapaapäivä.',
        'book_confirm': '{0} aloitti lukemaan kirjaa {1}. Ei sentään eroon oireista.',
        'book_nordin_confirm': '{0} aloitti lukemaan kirjaa {1}. MITÄ HELE?',
        'day_off': 'Hyvä on, {0}. Nauti vapaapäivästäsi.',
        'day_off_sick': 'Paranemisia {0}! https://www.youtube.com/watch?v=FktJFz-7s8U',
    }
